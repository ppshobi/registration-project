<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function show()
    {
        return view('register.begin')->with([
            'user' => session()->has('user') ? session('user'): new User(),
        ]);
    }

    public function store (Request $request)
    {
        $user = User::updateOrCreate([
            'phone' => $request->phone,
        ], [
            'first_name'   => $request->first_name,
            'last_name'    => $request->last_name,
        ]);

        session()->put('user', $user);

        return redirect(route('register.address'));
    }
}
