<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function show()
    {
        return view('register.address')->with([
            'address' =>  session('user') ? session('user')->address()->first() ?? new Address(): new Address(),
        ]);
    }

    public function store(Request $request)
    {
        Address::updateOrCreate([
            'user_id' => session('user')->id,
        ], [
            'house_number' => $request->house_number,
            'street'       => $request->street,
            'city'         => $request->city,
            'zip'          => $request->zip,
        ]);

        return redirect(route('register.account'));
    }
}
