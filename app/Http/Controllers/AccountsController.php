<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;
use App\Services\PaymentService;

class AccountsController extends Controller
{
    /**
     * @var PaymentService
     */
    private $paymentService;

    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    public function show()
    {
        return view('register.account')->with([
            'account' =>   session('user') ? session('user')->account()->first() ??  new Account()  : new Account(),
        ]);
    }

    public function store(Request $request)
    {
        $account = Account::updateOrCreate([
            'user_id' => session('user')->id,
        ], [
            'owner' => $request->owner,
            'iban'  => $request->iban,
        ]);

        if ($paymentResponse = $this->paymentService->notify($account)) {
            session('user')->update([
                'payment_data_id' => $paymentResponse->paymentDataId,
            ]);

            return view('register.success')->with([
                'payment_data_id' => $paymentResponse->paymentDataId,
            ]);
        }

        return response()
            ->view('register.failure')
            ->setStatusCode(500);
    }
}
