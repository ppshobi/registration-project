<?php

namespace App\Http\Middleware;

use Closure;

class SaveRegistrationProgress
{
    public function handle($request, Closure $next)
    {
        if (session()->has('user')) {
            session('user', session('user')->update([
                'current_route' => $request->route()->getName(),
            ]));
        }

        return $next($request);
    }
}
