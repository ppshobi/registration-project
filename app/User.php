<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function address()
    {
        return $this->hasOne(Address::class);
    }

    public function account()
    {
        return $this->hasOne(Account::class);
    }
}
