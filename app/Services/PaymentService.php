<?php


namespace App\Services;



use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class PaymentService
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function notify($account)
    {
        try {
            return json_decode($this->client->post(env('PAYMENT_NOTIFICATION_URL'), [
                'form_params' => [
                    "customerId" => $account->user_id,
                    "iban"       => $account->iban,
                    "owner"      => $account->owner,
                ],
            ])->getBody()->getContents());
        } catch (ServerException $e) {
            return false;
        }
    }
}