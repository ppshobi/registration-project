<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\RedirectIfNoUser;
use App\Http\Middleware\SaveRegistrationProgress;

Route::get('/register', function (){
    if ($user = session('user') && request()->route()->getName() != session('user')->current_route && session('user')->current_route) {
        return redirect(route(session('user')->current_route));
    }
    return redirect(route('register.begin'));
});

Route::group(['prefix' => 'register', 'middleware' => SaveRegistrationProgress::class], function () {
    Route::get('begin', 'RegisterController@show')->name('register.begin');
    Route::post('begin', 'RegisterController@store')->name('register.begin');

    Route::get('address', 'AddressController@show')->name('register.address')->middleware(RedirectIfNoUser::class);
    Route::post('address', 'AddressController@store')->name('register.address');

    Route::get('account', 'AccountsController@show')->name('register.account')->middleware(RedirectIfNoUser::class);
    Route::post('account', 'AccountsController@store')->name('register.account');
});
