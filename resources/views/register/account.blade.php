@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> Update Account </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register.account') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="owner" class="col-md-4 col-form-label text-md-right">Owner</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="owner" value="{{ $account->owner }}" required autofocus>
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="street" class="col-md-4 col-form-label text-md-right">IBAN</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="iban" value="{{ $account->iban }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4 row">
                                <a href="{{route('register.address')}}" class="btn btn-primary">
                                    Back
                                </a>
                                 <button type="submit" class="btn btn-primary ml-2">
                                    Next
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
