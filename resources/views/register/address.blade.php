@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> Update Address </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register.address') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="house_number" class="col-md-4 col-form-label text-md-right">House Number</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="house_number" value="{{ $address->house_number }}" required autofocus>
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="street" class="col-md-4 col-form-label text-md-right">Street</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="street" value="{{ $address->street }}" required autofocus>
                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">City</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="city" value="{{ $address->city }}" required autofocus>
                            </div>
                         </div>
                         <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">Zip Code</label>

                            <div class="col-md-6">
                                <input type="number" class="form-control" name="zip" value="{{ $address->zip }}" required autofocus>
                            </div>
                         </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4 row">
                                <a href="{{route('register.begin')}}" class="btn btn-primary">
                                    Back
                                </a>
                                 <button type="submit" class="btn btn-primary ml-2">
                                    Next
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
