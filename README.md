## Usage
1. Please clone this repository or download the project zip and `cd` into the project directory
2. Install the php dependencies using `composer install`
3. The asset files are already compiled and present inside `public/` directory so you wont need to run `npm install`
4. Run the application by running the command `php artisan serve`
5. Goto `localhost:8000/registration` to begin registering.

## A little bit about the application

This application is built using PHP 7.2 with the Laravel framework. For the database part I have used `sqlite` so that its very easy to run for the demo purpose.
The front end is simple bootstrap UI, with no javascript other than the javascript comes with bootstrap itself. The assets (css, javascript) is already compiled and present inside `/public` directory.

Our registration application is mostly server rendered traditional post back application. Each time a request is made to the server it makes appropriate changes in the database and the session, then it returns back the response.

We have 3 steps and after each step the changes are saved in the database, so that whenever this user comes back he can continue from where he stopped.

## Structure
Below are the important files from application, that too the important ones are present inside the `app/` directory

```
├── README.md
├── app
│   ├── Account.php
│   ├── Address.php
|   |── User.php
│   ├── Http
│   │   ├── Controllers
│   │   │   ├── AccountsController.php
│   │   │   ├── AddressController.php
│   │   │   ├── Controller.php
│   │   │   └── RegisterController.php
│   │   └── Middleware
│   │       ├── RedirectIfNoUser.php
│   │       ├── SaveRegistrationProgress.php
│   ├── Services
│       └── PaymentService.php
├── composer.json
├── composer.lock
├── .env
├── database
│   ├── database.sqlite
│   └── migrations
│       ├── 2014_10_12_000000_create_users_table.php
│       ├── 2020_02_23_233347_create_addresses_table.php
│       └── 2020_02_24_002618_create_accounts_table.php
├── database.sql
├── package-lock.json
├── package.json
├── public
│   ├── css
│   │   └── app.css
│   ├── favicon.ico
│   ├── index.php
│   ├── js
│   │   └── app.js
│   ├── mix-manifest.json
│   └── robots.txt
├── resources
│   ├── js
│   │   ├── app.js
│   │   ├── bootstrap.js
│   ├── sass
│   │   └── app.scss
│   └── views
│       ├── layouts
│       │   └── app.blade.php
│       ├── register
│       │   ├── account.blade.php
│       │   ├── address.blade.php
│       │   ├── begin.blade.php
│       │   ├── failure.blade.php
│       │   └── success.blade.php
│       └── welcome.blade.php
├── routes
│   └── web.php
├── storage
│   ├── framework
│      └── sessions
└── webpack.mix.js

```

 - Our application follows MVC pattern. 
 - The entry points are defined inside `routes/web.php`
 - The Models are present directly under the `app/` directory extending the `Model` classes
 - The Views or the templates are present inside the `resources/views` directory
 - The Controllers are present inside `app/Http/Controllers`
 - The Middleware's are present inside `app/Http/Middlewares`
 - I have created one service class named `PaymentService` under `app/Services` directory to abstract out our third party api communication logic.(The demo api specified in the task was responding with 500 status whenever I tried to POST to it but I have handled them in the `PaymentService` itself)
 - The Middleware's handles saving the information related to the progress and redirecting user.
 - The `.env` file holds the environment information (for the ease of running the application I have added it in the repository itself)
 - An export of the database is present in the root directory named `database.sql` also the table definitions can be found under `database/database.sqlite`
 
## Endpoints
  - `/register` - This endpoint starts the registration process or redirect the user to the last step.
  - `/register/begin` - Takes the basic user information such as `Name`
  - `/register/address` - Takes the address information
  - `/register/account` - Takes the account information and `POST` it to the external API and responds with either success(200) or failed (500) status.
 
## Notes
 - Since this task was mainly focused on the PHP side, I have used basically zero javascript. 
 - If I would redo it again? I would build an SPA and manage application states in the frontend itself. Maybe save the half baked information in the browser and call the backend at the end only once without hitting backend on every step.  
 - Since we lack the access control in this application I had to do some workaround with storing the information in the session, which also could have been avoided with using an SPA application, since we get a full object at the end.
 - As of now the `AccountsController` depends on a concrete instance of a `PaymentService` class. Better I could have made an `interface` and depend on the `PaymentInterface` rather than the concrete instance itself. But I think for this demo application that could have been an overkill, also if we decide to move that direction its only a matter of writing appropriate binding information to the container. 
 
## Screenshots
##### Step-1
![Step 1](01-begin.png)
##### Step-2
![Step 2](02-address.png)
##### Step-3
![Step 3](03-account.png)
##### Step-4
![Step 4](04-failed.png)